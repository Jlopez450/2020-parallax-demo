clock:
        clr d7
		clr d4
		move.b #$01, d4         ;BCD add by d4 (#1)
		move.b vblanks, d7
		abcd d4,d7              ;add binary coded decimal
        move.b d7, vblanks		;store result
	    cmp.b region, d7        ;see if we should a second
		 beq add_second
        rts	
add_second:
        move.b #$00, vblanks	
		move.b #$01, d4	
		move.b seconds, d7	
		abcd d4, d7
		move.b d7, seconds 	
		cmpi.b #$60, seconds
		 beq add_minute
		rts
add_minute:
        move.b #$00, seconds		
		move.b #$01, d4
		move.b minutes, d7	
		abcd d4, d7
		move.b d7, minutes
		cmpi.b #$60, minutes
		 bge reset_clock
		rts
reset_clock:
        move.b #$00,vblanks	
        move.b #$00,seconds		
        move.b #$00,minutes	
		rts					
region_check:
		clr d0
        move.b  $A10001, d0
		andi.b #$40, d0
		cmpi.b #$40, d0
		 beq pal 
		bra ntsc
pal:
		move.b #$50, region
		rts
ntsc:				
        move.b #$60, region
		rts		
