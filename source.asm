    include "ramdat.asm"
		      dc.l $000000,   start,     ErrorTrap, ErrorTrap
	          dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
      	      dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
	          dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
	          dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
	          dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
	          dc.l HBlank,    ErrorTrap, VBlank,    ErrorTrap
      	      dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
      	      dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.b "SEGA GENESIS    "
              dc.b "(C)  COPYRIGHT  "
              dc.b "PROGRAM NAME HERE                               "
              dc.b "PROGRAM NAME HERE                               "
              dc.b "GM 01234567-89"
              dc.w $DEAD        ;checksum
              dc.b "J               "
              dc.l 0
              dc.l ROM_End
              dc.l $FF0000
              dc.l $FFFFFF
              dc.b "    "
              dc.b "    "
              dc.b "    "
              dc.b "            "                           
              dc.b "This program contains blast processing! "
              dc.b "JUE             "
start:                          
        move.b  $A10001,d0
        andi.b  #$0F,d0
        beq.b   no_tmss       ;branch if oldest MD/GEN
        move.l  #'SEGA',$A14000 ;satisfy the TMSS        
no_tmss:
		move.w #$2700, sr       ;disable ints
		bsr clear_regs
		bsr clear_ram
        bsr setup_vdp
		bsr clear_vram
		move.w #$100,($A11100)	;steal z80's bus
		move.w #$100,($A11200)
		
		move.l #$40000010,(a3)	;reset vert scroll
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		
		lea font,a5    ;ASCII font
		move.w #$0bFF,d4 ;Graphics length in words
		move.l #$40000000,(a3);write to VRAM $0000
		bsr vram_loop	
		
		lea tile_sky,a5
		move.w #$29bf, d4
		move.l #$58000000,(a3)	;vram write 1800		
		bsr vram_loop
		
		lea pallettedata_sky, a5
		move.w #$f,d4
		move.l #$c0000000,(a3)	;pal 0
		bsr vram_loop
		
		lea mapdata_sky, a5
		move.w #$33f,d4
		move.w #$00c0,d1		;offset
		move.l #$60000003,(a3)	;vram e000 (B layer)
		bsr write_maps
		
		lea tile_mountain,a5
		move.w #$225f, d4
		move.l #$6b800001,(a3)	;vram write 6b80		
		bsr vram_loop
		
		lea pallettedata_mountain, a5
		move.w #$f,d4
		move.l #$c0200000,(a3)	;pal 1
		bsr vram_loop
		
		lea mapdata_mountain, a5
		move.w #$33f,d4
		move.w #$235c,d1		;offset
		move.l #$40000003,(a3)	;vram c000 (A layer)
		bsr write_maps
		
		lea tile_ground,a5
		move.w #$056f, d4
		move.l #$70400002,(a3)	;vram write b040		
		bsr vram_loop
		
		lea pallettedata_ground, a5
		move.w #$f,d4
		move.l #$c0400000,(a3)	;pal 2
		bsr vram_loop
		
		lea mapdata_ground, a5
		move.w #$3BF,d4
		move.w #$4582,d1		;offset
		move.l #$46800003,(a3)	;vram c680 (A layer)
		bsr write_maps	
		
		lea carsprite,a5
		move.w #$067f, d4
		move.l #$4e000003,(a3)	;vram write ce00	
		bsr vram_loop		

		lea pallettedata_car, a5
		move.w #$f,d4
		move.l #$c0600000,(a3)	;pal 3
		bsr vram_loop		
		bsr setsprite
		
		lea mus1+66,a6
		move.l a6,vgm_start
		
        move.w #$2300, sr       ;enable ints		
		
loop:
		move.b #$ff,vb_flag
		bsr music_driver
vb_wait:
		tst.b vb_flag
		bne vb_wait
		bra loop
		
setsprite:
		lea spritebuffer,a5		
		
		move.w #$014e,(a5)+
		move.w #$0501,(a5)+	;tire
		move.w #$66d0,(a5)+
		move.w #$00f7,(a5)+
		
		move.w #$0154,(a5)+
		move.w #$0502,(a5)+	;tire
		move.w #$76d0,(a5)+	;vflip for variety
		move.w #$0147,(a5)+


		move.w #$00f0,carposX
		move.w #$0130,carposY
		move.w #$0100,d0	;car Y sprite start position
		move.w #$0100,d1	;car X sprite start position
		move.w #$0203,d2	;size and link
		move.w #$6670,d3	;palette and tile number
		move.w #$f,d4
	
init_sprites1:	
		move.w d0,(a5)+
		move.w d2,(a5)+
		move.w d3,(a5)+
		move.w d1,(a5)+
		add.w #$01,d2	;incrament link
		add.w #$06,d3	;incrament tile number
		add.w #$08, d1	;lay tiles down in order
		dbf d4, init_sprites1
		
		move.w #$6673,d3
		move.w #$f,d4
		move.w #$0118,d0
		move.w #$0100,d1
init_sprites2:	
		move.w d0,(a5)+
		move.w d2,(a5)+
		move.w d3,(a5)+
		move.w d1,(a5)+
		add.w #$01,d2	;incrament link
		add.w #$06,d3	;incrament tile number
		add.w #$08, d1	;lay tiles down in order
		dbf d4, init_sprites2
		rts
		
anim_car:	;loop 1,2 for y, 3,4 for X
		move.w carposX,d0
		move.w carposY,d1
		lea spritebuffer+16,a5
		move.w #$0f,d4
animloop1:
		move.w d1,(a5)
		add.w #$08,a5	;next sprite
		dbf d4, animloop1
		add.w #$18,d1	;lower set of sprite parts
		move.w #$0f,d4
animloop2:
		move.w d1,(a5)
		add.w #$8,a5
		dbf d4, animloop2	
		
		lea spritebuffer+22,a5
		move.w #$0f,d4		
animloop3:
		move.w d0,(a5)
		add.w #$08,a5
		add.w #$08,d0	;position next sprite
		dbf d4, animloop3
	
		sub.w #$80,d0	;realign the bottom half
		move.w #$0f,d4		
animloop4:
		move.w d0,(a5)
		add.w #$08,a5
		add.w #$08,d0	;position next sprite
		dbf d4, animloop4
	
		move.w carposX,d0
		move.w carposY,d1	
		lea spritebuffer,a5	;now do the tires!
		add.w #$7,d0
		add.w #$1e,d1
		move.w d1,(a5)
		add.w #$06,a5
		move.w d0,(a5)
		move.w carposX,d0
		move.w carposY,d1	
		
		lea spritebuffer+8,a5	;now do the tires!
		add.w #$57,d0
		add.w #$23,d1
		move.w d1,(a5)
		add.w #$06,a5
		move.w d0,(a5)
		rts
		
read_controller:                ;d3 will have final controller reading!
		moveq	#0, d3            
	    moveq	#0, d7
	    move.b  #$40, ($A10009) ;Set direction
	    move.b  #$40, ($A10003) ;TH = 1
    	nop
	    nop
	    move.b  ($A10003), d3	;d3.b = X | 1 | C | B | R | L | D | U |
	    andi.b	#$3F, d3		;d3.b = 0 | 0 | C | B | R | L | D | U |
	    move.b	#$0, ($A10003)  ;TH = 0
	    nop
	    nop
	    move.b	($A10003), d7	;d7.b = X | 0 | S | A | 0 | 0 | D | U |
	    andi.b	#$30, d7		;d7.b = 0 | 0 | S | A | 0 | 0 | 0 | 0 |
	    lsl.b	#$2, d7		    ;d7.b = S | A | 0 | 0 | D | U | 0 | 0 |
	    or.b	d7, d3			;d3.b = S | A | C | B | R | L | D | U |
		rts	
		
write_maps:
		move.w (a5)+, d2
		add.w d1,d2				; add vram offset to tile
		move.w d2, (a4)
		dbf d4, write_maps
		rts

setup_vdp:
        lea    $C00004.l,a3     ;VDP control port
	    lea    $C00000.l,a4     ;VDP data port
        lea    (VDPSetupArray).l,a5
        move.w #0018,d4         ;loop counter
VDP_Loop:
        move.w (a5)+,(a3)       ;load setup array
        dbf d4,VDP_Loop
        rts  
vram_Loop:
        ;move.w (a5)+,(a4)
        move.w (a5)+,d2
        move.w d2,(a4)
        dbf d4,vram_Loop
        rts  
clear_regs:
		moveq #$00000000,d0
		move.l d0,d1
		move.l d0,d2
		move.l d0,d3
		move.l d0,d4
		move.l d0,d5
		move.l d0,d6
		move.l d0,d7
		move.l d0,a0
		move.l d0,a1
		move.l d0,a2
		move.l d0,a3
		move.l d0,a4
		move.l d0,a5
		move.l d0,a6
		rts
		
clear_ram:
		move.w #$7FF0,d0
		move.l #$FF0000,a0	
clear_ram_loop:
		move.w #$0000,(a0)+
		dbf d0, clear_ram_loop
		rts
clear_vram:
		move.l #$40000000,(a3)
		move.w #$7fff,d4
vram_clear_loop:
		move.w #$0000,(a4)
		dbf d4, vram_clear_loop
		rts		
		
update_sprites:
		move.l #$78000003,(a3)
		move.w #$ff,d4
		lea spritebuffer,a5
spriteloop:
		move.w (a5)+,(a4)
		dbf d4, spriteloop
		rts
		
input:
        move.b d3,d5    ;move d3 to d5 where it can be compared for input
		or.b #$fe,d5    ;$fe is controller reading if up is pressed
		cmpi.b #$fe,d5  ;check if up was pressed
		 beq up
chkdown:	    
		move.b d3,d5
		or.b #$fd,d5
		cmpi.b #$fd,d5
		 beq down
chkleft:		 
        move.b d3,d5
		or.b #$fb,d5
		 cmpi.b #$fb,d5
		 beq left
chkright:	    
		move.b d3,d5
		or.b #$f7,d5
		cmpi.b #$f7,d5
		 beq right			
chka:
		move.b d3,d5
		or.b #$bf,d5
		cmpi.b #$bf,d5
		 beq pressa		
chkb:
		move.b d3,d5
		or.b #$ef,d5
		cmpi.b #$ef,d5
		 beq pressb		
chkc:
		move.b d3,d5
		or.b #$df,d5
		cmpi.b #$df,d5
		 beq pressc		
		rts
		
up:
		sub.w #$01,carposY
		bra chkdown
down:
		add.w #$01,carposY
		bra chkleft	   
left:
		sub.w #$02,carposX
		bra chkright
right:
		add.w #$02,carposX
		bra chka
pressa:
		lea mus1+66,a6
		move.l a6,vgm_start
		lea mute,a6
		bra chkb
pressb:
		lea mus2+66,a6
		move.l a6,vgm_start
		lea mute,a6
		bra chkc
pressc:
		lea mus3+66,a6
		move.l a6,vgm_start
		lea mute,a6
		rts
		
spintires:
		eor.b #$ff,inverter1
		beq spin2
		lea spritebuffer+4,a5
		move.w (a5),d2
		add.w #$4,d2
		move.w d2, (a5)
		add.w #$8,a5
		move.w (a5),d2
		add.w #$4,d2
		move.w d2, (a5)
		rts
spin2:
		lea spritebuffer+4,a5
		move.w (a5),d2
		sub.w #$4,d2
		move.w d2, (a5)
		add.w #$8,a5
		move.w (a5),d2
		sub.w #$4,d2
		move.w d2, (a5)
		rts
		
ErrorTrap:        
        bra ErrorTrap

HBlank:
		add.w #$04,hblanks
		cmpi.w #$0068,hblanks
		blt scrollmountain
		cmpi.w #$0090,hblanks
		bgt scrollroad	
		
		;tst.b counter3
		;beq noline	;this block does line scroll ever other frame. Slower, but a bit choppier
		
		add.w #$01, scroll1
		move.w vblanks,d2
		sub.w d2,scroll1
		move.l #$7c000003,(a3)	;vram write fc00(A scroll)
		move.w scroll1,(a4)		
        rte
		
noline:
		move.w vblanks,d2
		sub.w d2,scroll1
		
		move.l #$7c000003,(a3)	;vram write fc00(A scroll)
		move.w scroll1,(a4)		
        rte

scrollroad:
		move.l #$7c000003,(a3)	;vram write fc00(A scroll)
		move.w scroll2,d2
		move.w d2,(a4)
		rte
		
scrollmountain:
		move.l #$7c000003,(a3)	;vram write fc00(A scroll)
		move.w scroll3,(a4)
		rte

VBlank:
		bsr read_controller
		bsr input
		bsr anim_car
		bsr spintires
		bsr update_sprites
		move.b #$00,vb_flag
		move.w #$00,hblanks
		eor.b #$ff,counter3
		add.w #$01,vblanks
		move.w #$00, scroll1
		sub.w #$06,scroll2	;road speed
		add.w #$01,counter1
		cmpi.w #$18,counter1	;mountain scroller
		blt nomountainscroll
		sub.w #$01,scroll3
		move.w #$00,counter1
nomountainscroll:
		add.w #$01,counter4
		cmpi.w #$ff,counter4	;sky scroller
		blt noskyscroll
		sub.w #$01,scroll4
		move.w #$00,counter4
		move.l #$7c020003,(a3)	;vram write fc02(B scroll)
		move.w scroll4,(a4)
		move.w scroll4,(a4)
noskyscroll:
        rte
		
return:
		rts
		
returnint:
		rte
	
 include "clock.asm"
 include "music_driver_V2.asm"
 include "data.asm"

ROM_End:
              
              